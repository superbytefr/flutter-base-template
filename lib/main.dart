//import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter_base_template/app/enums/environment.dart';
import 'package:flutter_base_template/app/myapp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future main() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  // uncomment for crashytics support
  //FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runApp(MyApp(environment: Environment.Production));
}