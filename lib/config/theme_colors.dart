import 'package:flutter/material.dart';

class ThemeColors {
  static const PrimaryColor = Color(0xFFF89A00);
  static const PrimaryColorLight = Color(0xFFFFC667);
  static const PrimaryColorDark = Color(0xFFFD811E);

  static const SecondaryColor = Color(0xFF0077CC);
  static const SecondaryColorLight = Color(0xFF45b2ff);
  static const SecondaryColorDark = Color(0xFF005CBF);

  static const Background = Color(0xFFffffff);
  static const TextColor = Color(0xFF343a40);

  static const BottomNavigationBarBorder = Colors.black12;
  static const BottomNavigationBarUnselected = Colors.grey;

  static const Black = Colors.black;
  static const Black1 = Color(0xFF343a40);
  static const Black2 = Colors.black87;
  static const Black3 = Colors.black26;

  static const Grey = Color(0xFFf6f7fb);
  static const GreyDark = Color(0xFFf3f5f4);
  static const GreyDarker = Color(0xFFd5dcd9);
}
