import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_template/config/routing/route_handlers.dart';

class Routes {
  Router _router;

  // definitions
  String root = "/";
  String app = "/app";

  String defaultRoute = "/app";

  List<String> _histories = [];

  Routes() {
    _router = Router();
    _configureRoutes();
  }

  void _configureRoutes() {
    _router.notFoundHandler = notFoundHandler;

    _router.define(root, handler: splashHandler);
    _router.define(app, handler: appHandler);
  }

  RouteFactory buildGenerator() {
    return _router.generator;
  }

  Future navigateTo(BuildContext context, String routeName) {
    if (_histories.last != routeName) {
      _histories.add(routeName);
    }

    print(_histories);

    return _router.navigateTo(context, routeName, transition: TransitionType.native);
  }

  void replace(BuildContext context, String routeName) {

    if (_histories.length > 0) {
      _histories.removeLast();
    }

    _histories.add(routeName);

    print(_histories);

    _router.navigateTo(context, routeName, transition: TransitionType.fadeIn, replace: true);
  }

  void pop(BuildContext context) {
    if (_histories.length > 0) {
      _histories.removeLast();
    }

    print(_histories);

    _router.pop(context);
  }

  void popOrReplace(BuildContext context, String routeName) {
    print("popOrReplace");
    print(_histories);

    if (_histories.length > 1) {
      pop(context);
    } else {
      replace(context, routeName);
    }
  }
}

