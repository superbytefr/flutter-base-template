import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_template/app/ui/page/app/app_page.dart';
import 'package:flutter_base_template/app/ui/page/notfound/notfound_page.dart';
import 'package:flutter_base_template/app/ui/page/splash/splash_page.dart';

// app
var appHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return AppPage();
    });

// splash
var splashHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return SplashPage();
    });

// not found
var notFoundHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      print("ROUTE WAS NOT FOUND !!!");

      return NotFoundPage(
          appTitle: "Not found",
          title: "Not found",
          message: "Page not found.",
          iconColor: Colors.green,
          context: context
      );
    });

/// Handles deep links into the app
/// To test on Android:
///
/// `adb shell am start -W -a android.intent.action.VIEW -d "fluro://deeplink?path=/message&mesage=fluro%20rocks%21%21" com.theyakka.fluro`
//var deepLinkHandler = new Handler(
//    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
//      String colorHex = params["color_hex"]?.first;
//      String result = params["result"]?.first;
//      Color color = new Color(0xFFFFFFFF);
//      if (colorHex != null && colorHex.length > 0) {
//        color = new Color(ColorHelpers.fromHexString(colorHex));
//      }
//      return new DemoSimpleComponent(
//          message: "DEEEEEP LINK!!!", color: color, result: result);
//    });