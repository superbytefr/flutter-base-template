class ApiRoutes {
  // Base Url
  static const BaseUrlDev = "http://localhost:3000";
  static const BaseUrlProd = "https://www.your-domain.com";

  static const Init = "api/init";
}