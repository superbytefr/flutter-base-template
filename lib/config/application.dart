import 'dart:ui';

// uncomment to activate analytics
//import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_base_template/app/enums/environment.dart';
import 'package:flutter_base_template/app/models/device_info.dart';
import 'package:flutter_base_template/app/models/user.dart';
import 'package:flutter_base_template/app/services/app_config_manager.dart';
import 'package:flutter_base_template/app/services/http_client.dart';
import 'package:flutter_base_template/app/services/storage_service.dart';
import 'package:flutter_base_template/config/routing/routes.dart';
import 'package:flutter_base_template/config/theme.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Application
{
  static const appName = "Template";
  static const appVersion = 1.0;
  static const defaultScreenWidth = 400.0;
  static const defaultScreenHeight = 810.0;
  static const defaultLocale = Locale("fr");
  static const supportedLocales = [
    Locale("fr"),
//    Locale("en", "FR"),
//    Locale("en", "US"),
  ];

  static final Routes routes = Routes();
  static final Theme theme = Theme();

  static DeviceInfo deviceInfo;

  // uncomment to activate analytics
  //static FirebaseAnalytics analytics = FirebaseAnalytics();

  static final HttpClient httpClient = HttpClient();
  static final StorageService storageService = StorageService();
  static final AppConfigManager appConfigManager = AppConfigManager();

  static final screen = ScreenUtil.getInstance();

  static User user;

  static Environment environment;
}