import 'package:flutter_base_template/app/enums/environment.dart';
import 'package:flutter_base_template/app/myapp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future main() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp(environment: Environment.Development));
}