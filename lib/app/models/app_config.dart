class AppConfig {
  bool hasAcceptedTermOfServices;
  bool hasNewMessages;

  AppConfig({
    this.hasAcceptedTermOfServices = false,
    this.hasNewMessages = false,
  });

  AppConfig.fromJson(Map<String, dynamic> json):
        hasAcceptedTermOfServices = json['has_accepted_term_of_services'],
        hasNewMessages = json['has_new_messages'];

  Map<String, dynamic> toJson() => {
    'has_accepted_term_of_services': hasAcceptedTermOfServices,
    'has_new_messages': hasNewMessages,
  };
}