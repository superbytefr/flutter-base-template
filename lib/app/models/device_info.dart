import 'package:device_info/device_info.dart';

class DeviceInfo {
  String name;
  String model;
  String uuid;
  String systemName;
  String systemVersion;

  DeviceInfo.fromAndroid(AndroidDeviceInfo info) :
      name = info.model.replaceAll(new RegExp('[^\u0001-\u007F]'),'_'),
      model = info.brand,
      systemName = "Android",
      systemVersion = info.version.release,
      uuid = info.androidId;

  DeviceInfo.fromIos(IosDeviceInfo info) :
        name = info.name.replaceAll(new RegExp('[^\u0001-\u007F]'),'_'),
        model = info.model,
        uuid = info.identifierForVendor,
        systemName = info.systemName,
        systemVersion = info.systemVersion;

  @override
  String toString() {
    return 'DeviceInfo{name: $name, model: $model, uuid: $uuid, systemName: $systemName, systemVersion: $systemVersion}';
  }
}