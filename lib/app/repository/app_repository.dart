import 'package:flutter_base_template/config/application.dart';
import 'package:flutter_base_template/config/routing/api_routes.dart';
import 'package:flutter/cupertino.dart';

class AppRepository {
  Future<dynamic> initialize(BuildContext context) async {

    var response = (await Application.httpClient.post(ApiRoutes.Init, []));

    return response;
  }
}