import 'dart:convert';

import 'package:flutter_base_template/app/models/app_config.dart';
import 'package:flutter_base_template/config/application.dart';

class AppConfigManager {

  static const String appConfigFileName = "config.data";

  Future<AppConfig> getAppConfig() async {
    AppConfig appConfig;

    try {
      String content = await Application.storageService.readFromTemp(appConfigFileName);
      Map<String, dynamic> data = jsonDecode(content);
      appConfig = AppConfig.fromJson(data);
    } catch (e) {
      appConfig = AppConfig();
    }

    return appConfig;
  }

  saveAppConfig(AppConfig appConfig) async{

    if (appConfig == null) {
      return;
    }

    String content = jsonEncode(appConfig);

    await Application.storageService.writeToTemp(content, appConfigFileName);
  }

  Future<AppConfig> reset() async {
    AppConfig defaultConfig = AppConfig();

    await saveAppConfig(defaultConfig);

    return defaultConfig;
  }
}