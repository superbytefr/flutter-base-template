import 'dart:convert';
import 'dart:io';

import 'package:flutter_base_template/app/enums/environment.dart';
import 'package:flutter_base_template/config/application.dart';
import 'package:flutter_base_template/config/routing/api_routes.dart';
import 'package:dio/dio.dart';

class HttpResponse {
  dynamic data;
  int statusCode;

  HttpResponse(this.data, this.statusCode);
}

class HttpClient {
  static final HttpClient _instance = HttpClient._internal();
  factory HttpClient() => _instance;
  Dio dio = new Dio();

  HttpClient._internal() {
    dio.options.connectTimeout = 10000;
    dio.options.receiveTimeout = 10000;
    dio.options.headers = _getRequiredHeaders();
  }

  Future<HttpResponse> get(String url) async {
    url = _buildUrl(url);
    Response response;

    try {
      response = await dio.get(url);
    } on DioError catch(e) {
      if (e.response != null)
        return HttpResponse(e.response.data, e.response.statusCode);

      return HttpResponse(e.message, 500);
    }

    return HttpResponse(response.data, response.statusCode);
  }

  Future<HttpResponse> post(String url, dynamic data) async {
    url = _buildUrl(url);
    Response response;

    try {
      response = await dio.post(url, data: data);
    } on DioError catch(e) {
      if (e.response != null)
        return HttpResponse(e.response.data, e.response.statusCode);

      return HttpResponse(e.message, 500);
    }

    return HttpResponse(response.data, response.statusCode);
  }

  Future<HttpResponse> postForm(String url, dynamic data, String namespace) async {
    url = _buildUrl(url);
    Response response;

    data[namespace] = json.encode(data[namespace]);
    FormData formData = FormData.fromMap(data);

    try {
      response = await dio.post(url, data: formData);
    } on DioError catch(e) {
      if (e.response != null)
        return HttpResponse(e.response.data, e.response.statusCode);

      return HttpResponse(e.message, 500);
    }

    return HttpResponse(response.data, response.statusCode);
  }

  Map<String, String> _getRequiredHeaders() {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.acceptHeader: 'application/json',
    };

    _addDeviceInfoToHeaders(headers);
    _addAuthenticationTokenToHeaders(headers);
    _addAppVersionToHeaders(headers);

    return headers;
  }

  void _addDeviceInfoToHeaders(Map<String, String> headers) {
    headers['device-name'] = Application.deviceInfo.name;
    headers['device-model'] = Application.deviceInfo.model;
    headers['device-uuid'] = Application.deviceInfo.uuid;
    headers['device-system-name'] = Application.deviceInfo.systemName;
    headers['device-system-version'] = Application.deviceInfo.systemVersion;
  }

  void _addAuthenticationTokenToHeaders(Map<String, String> headers) {
    if (Application.user == null || !Application.user.hasAccessToken()) {
      return;
    }

    headers[HttpHeaders.authorizationHeader] = "Bearer ${Application.user.accessToken}";
  }

  void _addAppVersionToHeaders(Map<String, String> headers) {

    headers['app-version'] = Application.appVersion.toString();
  }

  _buildUrl(String url) {
    var baseUrl = Application.environment == Environment.Development
        ? ApiRoutes.BaseUrlDev
        : ApiRoutes.BaseUrlProd;

    return "$baseUrl/$url";
  }
}