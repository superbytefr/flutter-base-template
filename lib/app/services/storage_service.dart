import 'dart:io';

import 'package:path_provider/path_provider.dart';

class StorageService {
  Future<File> write(String content, Directory dir, String fileName) async {
    final File file = File(dir.path + "/" + fileName);

    return file.writeAsString(content);
  }

  Future<File> writeToTemp(String content, String fileName) async {
    final tempDir = await getApplicationDocumentsDirectory();

    return write(content, tempDir, fileName);
  }

  Future<String> read(Directory dir, String fileName) async {
    final File file = File(dir.path + "/" + fileName);

    return file.readAsString();
  }

  Future<DateTime> getFileLastUpdate(Directory dir, String fileName) async {
    final File file = File(dir.path + "/" + fileName);
    final date = await file.lastModified();

    return date;
  }

  Future<String> readFromTemp(String fileName) async {
    final tempDir = await getApplicationDocumentsDirectory();

    return read(tempDir, fileName);
  }

  Future<File> getFileFromTemp(String fileName) async {
    final tempDir = await getApplicationDocumentsDirectory();

    return File(tempDir.path + "/" + fileName);
  }

  Future<DateTime> getFileLastUpdateFromTemp(String fileName) async {
    final tempDir = await getApplicationDocumentsDirectory();
    final file = File(tempDir.path + "/" + fileName);
    final fileExist = await file.exists();

    if (!fileExist)
      return null;

    final date = await file.lastModified();

    return date;
  }
}