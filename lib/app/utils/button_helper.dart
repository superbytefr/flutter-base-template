import 'package:flutter_base_template/config/theme_colors.dart';
import 'package:flutter/material.dart';

class ButtonHelper {
  static build({Widget child, onPressed}) {
    return RaisedButton(
      padding: const EdgeInsets.all(7),
      elevation: 0,
      onPressed: onPressed,
      splashColor: Colors.transparent,
      highlightColor: Colors.black12,
      disabledElevation: 0,
      highlightElevation: 0,
      child: child,
    );
  }

  static buildIcon({Widget label, Widget icon, onPressed}) {
    return RaisedButton.icon(
      elevation: 0,
      onPressed: onPressed,
      splashColor: Colors.transparent,
      highlightColor: Colors.black12,
      disabledElevation: 0,
      highlightElevation: 0,
      label: label,
      icon: icon,
    );
  }

  static buildOutline({Widget child, onPressed}) {
    return OutlineButton(
      padding: const EdgeInsets.all(7),
      onPressed: onPressed,
      splashColor: Colors.transparent,
      highlightColor: ThemeColors.SecondaryColor.withOpacity(0.1),
      borderSide: BorderSide(color: ThemeColors.SecondaryColor),
      highlightElevation: 0,
      child: child,
    );
  }

  static buildOutlineIcon({label, icon, onPressed}) {
    return OutlineButton.icon(
      padding: const EdgeInsets.all(7),
      onPressed: onPressed,
      splashColor: Colors.transparent,
      highlightColor: ThemeColors.SecondaryColor.withOpacity(0.1),
      borderSide: BorderSide(color: ThemeColors.SecondaryColor),
      highlightElevation: 0,
      label: label,
      icon: icon,
    );
  }

  static buildIconOnly({iconData, onPressed}) {
    return IconButton(
      padding: const EdgeInsets.all(7),
      onPressed: onPressed,
      splashColor: Colors.transparent,
      highlightColor: ThemeColors.SecondaryColor.withOpacity(0.1),
      icon: Icon(iconData, color: ThemeColors.SecondaryColor),
    );
  }

  static buildIconOnlyOutline({iconData, onPressed}) {
   return  Material(
        child: Ink(
          decoration: BoxDecoration(
            border: Border.all(color: ThemeColors.SecondaryColor), borderRadius: BorderRadius.all(Radius.circular(3.0)),
          ),
          child: InkWell(
            onTap:  onPressed,
            child: Padding(
              padding: const EdgeInsets.all(7),
              child: Icon(
                iconData,
                color: ThemeColors.SecondaryColor,
              ),
            ),
          ),
        )
    );
  }

  static buildBorderlessIcon({label, icon, onPressed}) {
    return FlatButton.icon(
      padding: const EdgeInsets.all(7),
      onPressed: onPressed,
      color: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: ThemeColors.SecondaryColor.withOpacity(0.1),
      label: label,
      icon: icon,
    );
  }
}