import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Translations.dart';

class DialogHelper {
  static info(BuildContext context, String title, String description) {
    if (Platform.isIOS)
      _showDialogIos(context, title, description);
    else
      _showDialogAndroid(context, title, description);
  }

  static action(BuildContext context, String title, String description, String actionText, Function action, {bool canIgnore=true, Function preventDismiss}) {
    if (Platform.isIOS)
      _showDialogIos(context, title, description, actionsTexts: [actionText], actions: [action], canIgnore: canIgnore, preventDismiss: preventDismiss);
    else
      _showDialogAndroid(context, title, description, actionsTexts: [actionText], actions: [action], canIgnore: canIgnore, preventDismiss: preventDismiss);
  }

  static actionCustom(BuildContext context, Widget title, Widget description, String actionText, Function action, {bool canIgnore=true, Function preventDismiss}) {
    if (Platform.isIOS)
      _showDialogIos(context, null, null, actionsTexts: [actionText], actions: [action],
          canIgnore: canIgnore, preventDismiss: preventDismiss, customContent: description, customTitle: title);
    else
      _showDialogAndroid(context, null, null, actionsTexts: [actionText], actions: [action],
          canIgnore: canIgnore, preventDismiss: preventDismiss, customContent: description, customTitle: title);
  }

  static binaryChoice(BuildContext context, String title, String description,
      String firstChoiceText, Function firstChoiceAction, String secondChoiceText, Function secondChoiceAction, {Function preventDismiss}) {
    if (Platform.isIOS)
      _showDialogIos(context, title, description, actionsTexts: [firstChoiceText, secondChoiceText], actions: [firstChoiceAction, secondChoiceAction], preventDismiss: preventDismiss);
    else
      _showDialogAndroid(context, title, description, actionsTexts: [firstChoiceText, secondChoiceText], actions: [firstChoiceAction, secondChoiceAction], preventDismiss: preventDismiss);
  }

  static binaryChoiceCustom(BuildContext context, Widget title, Widget description,
      String firstChoiceText, Function firstChoiceAction, String secondChoiceText, Function secondChoiceAction, {Function preventDismiss}) {
    if (Platform.isIOS)
      _showDialogIos(context, null, null, actionsTexts: [firstChoiceText, secondChoiceText], actions: [firstChoiceAction, secondChoiceAction],
          customContent: description, customTitle: title, preventDismiss: preventDismiss);
    else
      _showDialogAndroid(context, null, null, actionsTexts: [firstChoiceText, secondChoiceText], actions: [firstChoiceAction, secondChoiceAction],
          customContent: description, customTitle: title, preventDismiss: preventDismiss);
  }

  static Widget _getDialogContent(String description, Widget customContent) {
    if (customContent != null)
      return customContent;
    else if (description != null)
      return Text(description);
    else
      return Text("");
  }

  static Widget _getDialogTitle(String title, Widget customTitle) {
    if (customTitle != null)
      return customTitle;
    else if (title != null)
      return Text(title);
    else
      return Text("");
  }
  
  static _showDialogIos(BuildContext context, String title, String description, {List<String> actionsTexts, List<Function> actions,
    Widget customTitle, Widget customContent, bool canIgnore=true, Function preventDismiss}) {
    Widget content = _getDialogContent(description, customContent);
    Widget dialogTitle = _getDialogTitle(title, customTitle);
    Function willPop;

    if (preventDismiss != null)
      willPop = preventDismiss;
    else
      willPop = () async => true;

    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
            child: CupertinoAlertDialog(
                title: dialogTitle,
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      content
                    ],
                  ),
                ),
                actions: _buildIosOption(context, actionsTexts: actionsTexts, actions: actions, canIgnore: canIgnore)
            ),
            onWillPop: willPop,
          );
        }
    );
  }

  static _showDialogAndroid(BuildContext context, String title, String description, {List<String> actionsTexts, List<Function> actions,
      Widget customTitle, Widget customContent, bool canIgnore=true, Function preventDismiss}) {
    Widget content = _getDialogContent(description, customContent);
    Widget dialogTitle = _getDialogTitle(title, customTitle);
    Function willPop;

    if (preventDismiss != null)
      willPop = preventDismiss;
    else
      willPop = () async => true;

    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return WillPopScope(
            child: AlertDialog(
                title: dialogTitle,
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      content
                    ],
                  ),
                ),
                actions: _buildAndroidOption(context, actionsTexts: actionsTexts, actions: actions, canIgnore: canIgnore)
            ),
            onWillPop: willPop,
          );
        }
    );
  }

  static List<Widget> _buildIosOption(BuildContext context, {List<String> actionsTexts, List<Function> actions, bool canIgnore}) {
    List<Widget> actionTiles = [];

    if (actionsTexts == null || actionsTexts.isEmpty) {
      return <Widget>[
        CupertinoDialogAction(
          onPressed: () => Navigator.pop(context),
          child: Text(Translations.tr(context, "dialog.ok")),
        )
      ];
    }

    actionsTexts.asMap().forEach((int index, String actionText) {
      actionTiles.add(
        CupertinoDialogAction(
        onPressed: actions[index],
        child: Text(actionText),
      )
      );
    });

    if (canIgnore && actionsTexts.length == 1) {
      actionTiles.add(
          CupertinoDialogAction(
            onPressed: () => Navigator.pop(context),
            child: Text(Translations.tr(context, "dialog.ignore_btn")),
          )
      );
    }

    return actionTiles;
  }

  static List<Widget> _buildAndroidOption(BuildContext context, {List<String> actionsTexts, List<Function> actions, bool canIgnore}) {
    List<Widget> actionTiles = [];

    if (actionsTexts == null || actionsTexts.isEmpty) {
      return <Widget>[
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text(Translations.tr(context, "dialog.ok")),
        )
      ];
    }

    actionsTexts.asMap().forEach((int index, String actionText) {
      actionTiles.add(
        FlatButton(
          onPressed: actions[index],
          child: Text(actionText),
        ),
      );
    });

    if (canIgnore && actionsTexts.length == 1) {
      actionTiles.add(
          FlatButton(
            onPressed: () => Navigator.pop(context),
            child: Text(Translations.tr(context, "dialog.ignore_btn")),
          )
      );
    }

    return actionTiles;
  }
}
