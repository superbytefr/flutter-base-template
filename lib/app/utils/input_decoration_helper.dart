import 'package:flutter_base_template/config/theme_colors.dart';
import 'package:flutter/material.dart';

class InputDecorationHelper {
  static buildUnderline({intText, suffixText}) {
    return InputDecoration(
      suffixText: suffixText,
        focusedBorder: UnderlineInputBorder(
          borderSide:
          BorderSide(color: ThemeColors.PrimaryColor, width: 1),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
              color: ThemeColors.BottomNavigationBarBorder, width: 1),
        ),
        hintText: intText,
        border: UnderlineInputBorder(
          borderSide: BorderSide(
              color: ThemeColors.BottomNavigationBarBorder, width: 1),
        )
//              border: InputBorder.none,
    );
  }

  static buildBorderNone({intText}) {
    return InputDecoration(
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        hintText: intText,
        border: InputBorder.none);
  }
}