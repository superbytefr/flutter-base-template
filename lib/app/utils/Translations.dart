import 'dart:async';
import 'dart:convert';

import 'package:flutter_base_template/config/application.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;

class Translations {
  Translations(Locale locale) {
    this.locale = locale;
    _localizedValues = null;
  }

  Locale locale;
  static Map<dynamic, dynamic> _localizedValues;


  static tr(BuildContext context, String key) {
    return of(context).text(key);
  }

  static Translations of(BuildContext context) {
    return Localizations.of<Translations>(context, Translations);
  }

  static Locale currentLocale(BuildContext context) {
    return Localizations.localeOf(context);
  }

  String text(String key) {

    var keys = key.split('.');
    var value = _localizedValues;
    var finalText;

    for (var elem in keys) {
      if (value[elem] is String) {
        finalText = value[elem];
        break;
      }

      value = value[elem];
    }

    if (finalText != null) {
      return finalText;
    }

    return _localizedValues[key] ?? '* $key not found';
  }

  static Future<Translations> load(Locale locale) async {
    Translations translations = new Translations(locale);
    String jsonContent =
    await rootBundle.loadString("assets/locales/${locale.languageCode}.json");
    _localizedValues = json.decode(jsonContent);
    return translations;
  }

  get currentLanguage => locale.languageCode;
}

class TranslationsDelegate extends LocalizationsDelegate<Translations> {
  const TranslationsDelegate();

  @override
  bool isSupported(Locale locale) => Application.supportedLocales.contains(locale);

  @override
  Future<Translations> load(Locale locale) => Translations.load(locale);

  @override
  bool shouldReload(TranslationsDelegate old) => false;
}