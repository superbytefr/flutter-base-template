import 'package:flutter_base_template/app/enums/view_state.dart';
import 'package:flutter_base_template/app/models/app_config.dart';
import 'package:flutter_base_template/app/models/device_info.dart';
import 'package:flutter_base_template/config/application.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:io' show Platform;

class AppScopedModel extends Model {

  AppConfig _config;
  AppConfig  get config => _config;

  Locale _currentLanguage;
  Locale get currentLanguage => _currentLanguage;

  ViewState _appState = ViewState.Idle;
  ViewState get appState => _appState;



  void initializeApp(BuildContext context) {
    _initializeDeviceInfo(context);
    _initializeAppConfig();
  }

  void _initializeAppConfig() async {
    _config = await Application.appConfigManager.getAppConfig();
  }

  _initializeDeviceInfo(BuildContext context) {
    setAppState(ViewState.Busy);

    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      deviceInfo.androidInfo.then((info) {
        Application.deviceInfo = DeviceInfo.fromAndroid(info);

        setAppState(ViewState.Success);

      });
    } else {
      deviceInfo.iosInfo.then((info) {
        Application.deviceInfo = DeviceInfo.fromIos(info);

        setAppState(ViewState.Success);

      });
    }
  }



  void setAppState(ViewState newState) {
    _appState = newState;
    notifyListeners();
  }

  static AppScopedModel of(BuildContext context) =>
      ScopedModel.of<AppScopedModel>(context, rebuildOnChange: true);
}