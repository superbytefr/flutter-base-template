// uncomment to activate analytic observer
//import 'package:firebase_analytics/observer.dart';
import 'package:flutter_base_template/app/enums/environment.dart';
import 'package:flutter_base_template/app/scoped_models/app_scoped_model.dart';
import 'package:flutter_base_template/app/utils/Translations.dart';
import 'package:flutter_base_template/config/application.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';

class MyApp extends StatelessWidget {
  AppScopedModel _appScopedModel;
  final global = GlobalKey();

  MyApp({Environment environment = Environment.Production}) {
    Application.environment = environment;
    _appScopedModel = AppScopedModel();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel(
      model: _appScopedModel,
      child: MaterialApp(
        key: global,
        debugShowCheckedModeBanner: Application.environment == Environment.Development,
        title: Application.appName,
        theme: Application.theme.defaultTheme,
        initialRoute: Application.routes.root,
        onGenerateRoute: Application.routes.buildGenerator(),
        localizationsDelegates: [
          const TranslationsDelegate(),
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: Application.supportedLocales,
        locale: Application.defaultLocale,
        // uncomment to activate analytics observer
        /*navigatorObservers: [
          FirebaseAnalyticsObserver(analytics: Application.analytics),
        ],*/
      ),
    );
  }

}
