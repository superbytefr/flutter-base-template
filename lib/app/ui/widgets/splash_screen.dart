import 'package:flutter_base_template/config/application.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  final bool isLoading;
  final Widget container;
  final String next;

  SplashScreen({this.container, this.next, this.isLoading});

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _isLoading = true;

  @override
  void initState() {
    _isLoading = widget.isLoading ?? true;
    super.initState();
  }

  @override
  void didUpdateWidget(SplashScreen oldWidget) {
    if (widget.isLoading != null && widget.isLoading != oldWidget.isLoading) {
      setState(() {
        _isLoading = widget.isLoading;

        _handleFinish();
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return widget.container;
  }

  _handleFinish() {
    if (_isLoading) {
      return;
    }

    Future.delayed(Duration(milliseconds: 100)).then((_) =>
        Application.routes.replace(context, widget.next)
    );
  }
}
