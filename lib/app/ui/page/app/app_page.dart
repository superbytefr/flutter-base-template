import 'package:flutter_base_template/app/scoped_models/app_scoped_model.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AppPage extends StatefulWidget {

  AppPage({Key key}) : super(key: key);

  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  AppScopedModel _appScopedModel;

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    _appScopedModel = ScopedModel.of<AppScopedModel>(context, rebuildOnChange: true);

    return Scaffold(
      body: PageStorage(child: SafeArea(child: SingleChildScrollView()), bucket: bucket),
    );
  }
  
}
