import 'package:flutter_base_template/app/enums/view_state.dart';
import 'package:flutter_base_template/app/scoped_models/app_scoped_model.dart';
import 'package:flutter_base_template/app/ui/widgets/splash_screen.dart';
import 'package:flutter_base_template/config/application.dart';
import 'package:flutter_base_template/config/theme_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  AppScopedModel appScopedModel;
  BuildContext context;

  @override
  void initState() {
    super.initState();

    initializeApplication();
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    this.appScopedModel = AppScopedModel.of(context);

    initializeScreenUtil(context);

    return SplashScreen(
      container: getSplashContainer(),
      next: Application.routes.defaultRoute,
      isLoading: appScopedModel.appState != ViewState.Success,
    );
  }

  void initializeApplication() {
    Future.delayed(Duration.zero).then((_) {
      appScopedModel.initializeApp(context);
    });
  }

  getSplashContainer() {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset('assets/images/logo_splash.png', scale: 10)
                  ],
                )),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(ThemeColors.PrimaryColor),
                      strokeWidth: 1.5,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void initializeScreenUtil(BuildContext context) {
    ScreenUtil.instance = ScreenUtil(
      width: Application.defaultScreenWidth,
      height: Application.defaultScreenHeight,
      allowFontScaling: true,
    )..init(context);

  }
}
