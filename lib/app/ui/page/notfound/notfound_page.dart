import 'package:flutter_base_template/app/ui/widgets/profile_tile.dart';
import 'package:flutter_base_template/config/theme_colors.dart';
import 'package:flutter/material.dart';

class NotFoundPage extends StatelessWidget {
  final appTitle;
  final title;
  final message;
  final IconData icon;
  final String image;
  final String buttonLabel;
  final Color backgroundColor;
  final Color textColor;
  final iconColor;
  final context;

  NotFoundPage(
      {this.appTitle = "Search",
        this.title = "No Result",
        this.message = "Try a more general keyword.",
        this.buttonLabel = "Back",
        this.backgroundColor = ThemeColors.Background,
        this.textColor = Colors.black,
        this.icon = Icons.search,
        this.image,
        this.iconColor = Colors.black,
        this.context});

  Widget bodyButton() {
    if (this.buttonLabel == null)
      return Container();
    return  RaisedButton(
        onPressed: () { onButtonPressed(); },
        child: Text(this.buttonLabel)
    );
  }

  Widget bodyData() => Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          icon,
          size: 100.0,
          color: iconColor,
        ),
        SizedBox(
          height: 20.0,
        ),
        ProfileTile(
          title: title,
          subtitle: message,
          textColor: this.textColor,
        ),
        Padding(padding: const EdgeInsets.all(10.0)),
        bodyButton()
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(appTitle),
      ),
      body: bodyData(),
      bottomNavigationBar: BottomAppBar(
        child: Container(height: 50.0,),
      ),
      backgroundColor: this.backgroundColor,
    );
  }

  void onButtonPressed() {
    Navigator.of(context).pop();
  }
}